<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
})->name('home');

Route::get('/about', function () {
    return view('layouts.about');
})->name('about');

Route::get('/property', function () {
    return view('layouts.property');
})->name('property');

Route::get('/blog', function () {
    return view('layouts.blog');
})->name('blog');

Route::get('/contact', function () {
    return view('layouts.contact');
})->name('contact');

//....................... Pages ......................

Route::get('/property-single', function () {
    return view('layouts.pages.property-single');
})->name('property-single');

Route::get('/blog-single', function () {
    return view('layouts.pages.blog-single');
})->name('blog-single');

Route::get('/agents-grid', function () {
    return view('layouts.pages.agents-grid');
})->name('agents-grid');

Route::get('/agent-single', function () {
    return view('layouts.pages.agent-single');
})->name('agent-single');
